# CERISICPoster

This repository is a LaTeX package designed to help CeREF researchers writing their A0 posters. A usage example is given in the file `example_CeREF.tex`

## Package Installation

The simplest way to use this package, is to use it as a local package. To do so, download the repository and edit the `example_CeREF.tex` file (or any other given example file that best fit your needs).

## Usage

### Load the package

In order to use the package, you will have load the package before the \begin{document} command, as below:
\usepackage[]{CeREFPoster}


### Package options

Select if the poster is either portrait or landscape with one of these options:
* portrait
* landscape

```
Example:
\usepackage[portrait]{CeREFPoster}

```

Define the domain:
* ceref
* arts
* education
* economique
* social
* agronomique
* sante
* technique

```
Example:
\usepackage[portrait, technique]{CeREFPoster}
```

Then you can add all the different partners to the poject:
* promoter
* industrialpartner
* scientificpartner
* fundingpartner

```
Example:
\usepackage[portrait, promoter, industrialpartner]{CeREFPoster}
```

You can also change the number of columns with one of these options:
* twocolumn
* threecolumn
* fourcolumn

```
Example:
\usepackage[twocolum]{CeREFPoster}
```

You can add a column divider rule by using the option `columnsep`
```
Example:
\usepackage[twocolum,columnsep]{CeREFPoster}
```

If you have a lot of partners in your projet, and you need to add all of the logo, this can be done by using the option `additionalpartner`. This option will allow you to have an extra space below the standard partner, to add an image spread on all the columns. This option will also allow you to use the command `additionalpartner{}` to specify the image.
```
Example:
\usepackage[twocolum,promoter, scientificpartner, additionalpartner]{CeREFPoster}

\additionalpartner{\includegraphics[<image size>]{<image path>}}
```
Remark: Keep in mind that this image should be way more longer than high.


### Before \begin{document}
This package will also define several commands:

* \postertitle{}
* \postersubtitle{}
* \authors{}
* \institutes{}

You can also define the contact information with the following command:
* \contact{}


* \promoter{}
* \industrialpartner{}
* \scientificpartner{}
* \fundingpartner{}



### After \begin{document}
This package will also give you acces to 3 main environments:

* \begin{header} \end{header}
* \begin{body} \end{body}
* \begin{footer} \end{footer}

The package will automatically balance the different columns. If you want the columns to be unbalanced use the `\columnbreak` command.
